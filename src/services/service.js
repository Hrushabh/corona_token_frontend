import http from "../config";

class DataService {

  async login(data){
    const response = await http.post("/login", data);
    if (response.data.accessToken) {
      localStorage.setItem("user", JSON.stringify(response.data));
    }
    return response.data;;
  }

  logout() {
    localStorage.removeItem("user");
  }

  getCurrentUser() {
    return JSON.parse(localStorage.getItem('user'));;
  }

  signup(data) {
    return http.post("/register", data);
  }
  getPlace(id) {
    return http.get(`/places/${id}`);
  }
}

export default new DataService();