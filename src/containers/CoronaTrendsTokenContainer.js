import React, {Component} from 'react';
import CoronaTrendsComponent from '../components/CoronaTrendsComponent.js';
import Footer from '../components/footer/footer.js';
import Header from '../components/Header.js';
class CoronaTrendsTokenComponent extends Component {
    constructor(){
        super();
        this.state = {};
    }

    render(){
        return(
            <div>
              <Header />
              <div className = "content-offset">
                <CoronaTrendsComponent />
              </div>
              <Footer />
            </div>


        );
    }
}

export default CoronaTrendsTokenComponent;