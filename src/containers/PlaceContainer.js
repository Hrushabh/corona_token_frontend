import React, {Component} from 'react';
import Header from '../components/Header.js';
import Footer from '../components/footer/footer.js';
import PlacesComponent from '../components/MUPlaces/placescontainer.js';
import CoronaAnimation from '../components/CoronaAnimation/CoronaAnimation.js';

class PlacesContainer extends Component{
  constructor(){
    super();
    this.state = {
        animationOver: false
    };
  }

  componentDidMount(){
    setTimeout(function(){
      this.setState((prevState) => {
        return {animationOver: !prevState.animationOver}
      });
    }.bind(this), 8500)
  }

  render(){
    return(
      <div>
        <Header />
        <div>
          <CoronaAnimation />
          <div className = "content-offset">
            { this.state.animationOver ? <PlacesComponent /> : null }
          </div>
        </div>
        { this.state.animationOver ? <Footer/> : null }
      </div>
    );
  }
}

export default PlacesContainer;