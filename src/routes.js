import React from 'react';
import { Route, Switch, Redirect } from "react-router-dom";
import CoronaTrendsToken from './containers/CoronaTrendsTokenContainer.js'
import Register from './components/register.js';
import Login from './components/login.js';
import PlacesContainer from './containers/PlaceContainer.js';
import Places from './components/Places/individualPlace.js';

export default (
  <Switch>
    <Route exact path="/">
      <Redirect push to="/home" />
    </Route>
    <Route exact path = "/" component = {PlacesContainer} />
    <Route exact path = "/home" component = {PlacesContainer} />
    <Route exact path = "/register" component = {Register} />
    <Route exact path = "/login" component = {Login} />
    <Route exact path = "/coronatrends" component = {CoronaTrendsToken} />
    <Route path="/places/:id" component={Places} />
  </Switch>
);