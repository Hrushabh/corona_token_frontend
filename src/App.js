import React, { Component } from 'react';

import "bootstrap/dist/css/bootstrap.min.css";
import "shards-ui/dist/css/shards.min.css"

import './styles/_custom.scss';
import Routes from './routes.js';

class App extends Component {

  render(){
    return (
      <div>
        {Routes}
      </div>
      
    );
  }
}
export default App;
