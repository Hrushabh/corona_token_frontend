import React, { useState } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
//import FormControlLabel from '@material-ui/core/FormControlLabel';
//import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import PersonRoundedIcon from '@material-ui/icons/PersonRounded';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import DataService from "../services/service";
import { useHistory } from "react-router-dom";

/////////////////////// Function to display website copyright(can be reused) ////////////////
function Copyright() {
  return (
    <Typography variant="body2" color="textPrimary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="/">
        Team LASH
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}
//////////////////////////////////////////////////////////////////

function validateEmail(email) {
  const re = /\S+@\S+\.\S+/;
  return re.test(String(email).toLowerCase());
}

/////////////////////// CUSTOM STYLES ///////////////////////////
const useStyles = makeStyles((theme) => ({
  root: {
    height: '100vh',
  },
  image: {
    backgroundImage: 'url(https://source.unsplash.com/random)',
    backgroundRepeat: 'no-repeat',
    backgroundColor:
      theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
    backgroundSize: 'cover',
    backgroundPosition: 'center',
  },
  paper: {
    margin: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    // backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));
////////////////////////////////////////////////////
const Input = props => (
  <TextField
      error={ props.error }
      type={ props.type }
      label= { props.label }
      value={ props.value }
      onChange={ props.onChangeFunc }
      helperText={ props.error ? props.errMsg : props.normalMsg}
      variant="outlined"
      margin="normal"
      size="small"
      fullWidth
  />
);

export default function SignInSide() {
    const classes = useStyles();
    const history = useHistory();
    ////////////////////////  Defination for fields /////////////////////
    const [email, setEmail] = useState('');
    const [emailErr, setEmailErr] = useState(false);
    const [password, setPassword] = useState('');
    const [passwordErr, setPasswordErr] = useState(false);

    ////// will be used if logged in ///////////////
    //const [confirmation, setConfirmation] = useState(false);
    ///////////////////////////////////////////////////////////////////

    ///////////////////// LOGIN FUNCTIONALITY /////////////////////////
    function handleClick(e) {
      e.preventDefault();
      if (!validateEmail(email.trim()) || !password.trim()) {
            if (!validateEmail(email.trim())) setEmailErr(true);
            else setEmailErr(false);
            if (!password.trim()) setPasswordErr(true);
            else setPasswordErr(false);
      }
      else{
          // setConfirmation(true);
          var data = {
              email: email,
              password: password
          }
          DataService.login(data)
              .then(response => {
                  console.log(response)
                  history.push('/home');
          })
          .catch(e => {
              console.log(e);
          });
      }
    }
    ///////////////////////////////////////////////////////////////////

  return (
    <Grid container component="main" className={classes.root}>
      <CssBaseline />
      <Grid item xs={false} sm={4} md={8} className={classes.image} />
      <Grid item xs={12} sm={8} md={4} component={Paper} elevation={6} square>
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <PersonRoundedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign in
          </Typography>
          <form className={classes.form} noValidate autoComplete="off">
            <Input 
              error={emailErr} 
              type="text"
              label="Email Address"
              value={ email } 
              onChangeFunc={e=>setEmail(e.target.value)}
              errMsg="Please enter a valid email address"
              normalMsg=""
              autoFocus
            />
            <Input 
              type="password"
              error={passwordErr} 
              label="Password"
              value={ password } 
              onChangeFunc={e=>setPassword(e.target.value)}
              errMsg="Please enter password"
              normalMsg=""
            />
            <Button
              type="submit"
              // fullwidth
              variant="contained"
              color="primary"
              className={classes.submit}
              onClick={handleClick}
              disableElevation
            >
              Sign In
            </Button>
            
            <Grid container>
              <Grid item xs>
                <Link href="#" variant="body2">
                  Forgot password?
                </Link>
              </Grid>
              <Grid item>
                <Link href="/register" variant="body2">
                  {"Don't have an account? Sign Up"}
                </Link>
              </Grid>
            </Grid>
            <Box mt={5}>
              <Copyright />
            </Box>
          </form>
        </div>
      </Grid>
    </Grid>
  );
}