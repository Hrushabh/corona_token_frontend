import React, { Component } from 'react';
import '../CoronaAnimation/CoronaAnimation.css'
import VirusBlue from '../virus/VirusBlue'
import VirusGreen from '../virus/VirusGreen'
import VirusBrown from '../virus/VirusBrown'
import VirusPurple from '../virus/VirusPurple'

class CoronaAnimation extends Component{

   
    constructor(props)
    {
       super(props)
    }

    componentDidMount(){
       setTimeout(function(){
           document.getElementById("vir1").classList.add("fadeOut")
       }, 4000)
       setTimeout(function(){
            document.getElementById("vir6").classList.add("fadeOut")
        }, 4200)
        setTimeout(function(){
            document.getElementById("vir7").classList.add("fadeOut")
            document.getElementById("vir4").classList.add("fadeOut")
            document.getElementById("vir3").classList.add("slideLeft")
        }, 4500)
        setTimeout(function(){
            document.getElementById("vir2").classList.add("slideLeft")
            document.getElementById("vir5").classList.add("slideLeftSpl")
        }, 4800)
        setTimeout(function(){
            document.getElementById("heading").classList.add("fadeIn")
        }, 5500)
    }

    render() {
        return(
            <div id="virPanel" style={{display:"inline-flex", position:"relative", marginTop: "2%", marginLeft: "10%", width: "100%"}} >
            <div id="vir1"><VirusBlue wait={2000}/>   </div> 
            <div id="vir2"><VirusPurple wait={1000}/></div>
            <div id="vir3"><VirusBrown wait={2000}/></div>
            <div id="vir4"><VirusGreen wait={1500}/></div>
            <div id="vir5"><VirusBlue wait={1200}/></div>
            <div id="vir6"><VirusBrown wait={800}/></div>
            <div id="vir7"><VirusPurple wait={1700}/></div>
            <div id="heading" style={{margin: "5%", opacity: "0"}}>
                <h1>TOKEN CORONA</h1>
                <p>
                    Maintain social distancing while you visit your favorite destinations!
                    <br/>
                    Using our token system, you can be sure where it is not going to be crowded.
                    <br/>
                    <a href="#"><u>Click here</u></a> to look at current COVID-19 stats.
                </p>
            </div>
            </div>
           
        )
    }
}

export default CoronaAnimation