import React, { Component } from "react";
import DataService from "../../services/service";

class individualPlace extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentPlace: {
                _id:null,
                place:"",
                weeklyTokens:null,
                dailyTokens:null,
                currentTokens:null,
                url1:"",
                url2:"",
                url3:"",
                url4:"",
                url5:"",
                description:""
                }
      };
    }
    componentDidMount() {
        this.getPlace(this.props.match.params.id);
      }
    getPlace(id) {
        DataService.getPlace(id)
          .then(response => {
            this.setState({
              currentPlace: response.data
            });
            console.log(response.data);
          })
          .catch(e => {
            console.log(e);
          });
      }
    render()
    {
        const { currentPlace } = this.state;
        return(
        <h1>{currentPlace.place}</h1>
        )
    }
}

export default individualPlace;