import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import { Link } from "react-router-dom";
import './row.css';

class row extends Component {
    render()
    {
        const rdata=this.props.data;
    return (
      <React.Fragment>
        <Grid item xs={6} sm={3}>
            <div className="flip-card">
                <Link to={"/places/"+rdata.id[0]}>
                    <div className="flip-card-inner">
                        <div className="flip-card-front" style={{ backgroundImage: `url(${require("../images/"+rdata.image[0]+".jpg")})` }}>
                        </div>
                        <div className="flip-card-back">
                            <p>{rdata.name[0]}</p> 
                            <p>{rdata.description[0]}</p>
                        </div>
                    </div>
                </Link>
            </div>
        </Grid>
        <Grid item xs={6} sm={3}>
            <div className="flip-card">
                <Link to={"/places/"+rdata.id[1]}>
                    <div className="flip-card-inner">
                        <div className="flip-card-front" style={{ backgroundImage: `url(${require("../images/"+rdata.image[1]+".jpg")})` }}>
                        </div>
                        <div className="flip-card-back">
                            <p>{rdata.name[1]}</p> 
                            <p>{rdata.description[1]}</p>
                        </div>
                    </div>
                </Link>
            </div>
        </Grid>
        <Grid item xs={6} sm={3}>
            <div className="flip-card">
                <Link to={"/places/"+rdata.id[2]}>
                    <div className="flip-card-inner">
                        <div className="flip-card-front" style={{ backgroundImage: `url(${require("../images/"+rdata.image[2]+".jpg")})` }}>
                        </div>
                        <div className="flip-card-back">
                            <p>{rdata.name[2]}</p> 
                            <p>{rdata.description[2]}</p>
                        </div>
                    </div>
                </Link>
            </div>
        </Grid>
        <Grid item xs={6} sm={3}>
            <div className="flip-card">
                <Link to={"/places/"+rdata.id[3]}>
                    <div className="flip-card-inner">
                        <div className="flip-card-front" style={{ backgroundImage: `url(${require("../images/"+rdata.image[3]+".jpg")})` }}>
                        </div>
                        <div className="flip-card-back">
                            <p>{rdata.name[3]}</p> 
                            <p>{rdata.description[3]}</p>
                        </div>
                    </div>
                </Link>
            </div>
        </Grid>
      </React.Fragment>
    );
  }
}

export default row 