import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import Row from './row';
import './placescontainer.css';

class placescontainer extends Component {

  render()
  {
        const r1 = { name: ["Santa Monica Beach","Venice Beach","Long Beach","Malibu Beach"],
            image:["SantaMonica","VeniceBeach","LongBeach","MalibuBeach"],
            description:["Santa Monica Beach Description","Venice Beach Description","Long Beach Description","Malibu Beach Description"],
            id:["1","2","3","4"]
        }
        const r2 = { name: ["Manhattan Beach","Redondo Beach","Hermosa Beach","El Matador"],
            image:["ManhattanBeach","RedondoBeach","HermosaBeach","ElMatador"],
            description:["Manhattan Beach Description","Redondo Beach Description","Hermosa Beach Description","El Matador Description"],
            id:["5","6","7","8"]
        }
        const r3 = { name: ["El Segundo","Lake Balboa","Lake Hughes","Griffith Observatory"],
            image:["ElSegundo","LakeBalboa","LakeHughes","GriffithObservatory"],
            description:["El Segundo Description","Lake Balboa Description","Lake Hughes Description","Griffith Observatory Description"],
            id:["9","10","11","12"]
        }
        const r4 = { name: ["LACMA","Natural History Museum","Eaton Canyon","The Grove"],
            image:["LACMA","NaturalHistoryMuseum","EatonCanyon","TheGrove"],
            description:["LACMA Description","Natural History Museum Description","Eaton Canyon Description","The Grove Description"],
            id:["13","14","15","16"]
        }
    return (
      <div className='parent'>
            <Grid container spacing={3}>
                <Row data={r1}/>
            </Grid>
            <Grid container spacing={3}>
                <Row data={r2}/>
            </Grid>
            <Grid container spacing={3}>
                <Row data={r3}/>
            </Grid>
            <Grid container spacing={3}>
                <Row data={r4}/>
            </Grid>
      </div>
    );
  }
}
export default placescontainer