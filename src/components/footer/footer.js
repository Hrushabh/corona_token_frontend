import React, { Component } from 'react';
import './footer.css';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
import DataService from '../../services/service';

function Copyright() {
    return (
      <Typography variant="body2" align="center">
        {'Copyright © '}
        <Link color="inherit" href="/">
          Team LASH
        </Link>{' '}
        {new Date().getFullYear()}
        {'.'}
      </Typography>
    );
  }

  class footer extends Component{
    constructor(props) {
        super(props);
        this.logOut = this.logOut.bind(this);
    
        this.state = {
          currentUser: undefined
        };
      }
    
      componentDidMount() {
        const user = DataService.getCurrentUser();
    
        if (user) {
          this.setState({
            currentUser: user
          });
        }
      }
    
      logOut() {
        DataService.logout();
      }

    render() {
        const { currentUser} = this.state;
            return (
            <div className="text-center text-md-left footerMainDiv">
                {/* link for the social media buttons */}
                <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />

                <div className="footerSubDiv">
                    {/* footer info division to write something about*/}
                    <div className="col-md-4 mx-auto">
                        <h5 className="font-weight-bold text-uppercase mt-3 mb-4">Footer Title</h5>
                        <p>Here you can use rows and columns to organize your footer content. Lorem ipsum dolor sit amet,
                    consectetur adipisicing elit.</p>
                    </div>

                    {/* footer links div */}
                    <div className="col-md-2 mx-auto">
                    { currentUser ? (
                        <ul className="footerLinks">
                            <li><a href="/home">Home</a></li>
                            <li><a href="/about">About</a></li>
                            <li><a href="/home"  onClick={this.logOut}>LogOut</a></li>
                        </ul>
                     ) : (
                        <ul className="footerLinks">
                        <li><a href="/home">Home</a></li>
                        <li><a href="/about">About</a></li>
                        <li><a href="/register">Register</a></li>
                        <li><a href="/login">Login</a></li>
                    </ul>
                     )}
                    </div>
                </div>
                <hr />
                {/* footer social media icons and links */}
                <ul className="socialMediaUL">
                    <li className="socialMediaAlign">
                        <a href="http://www.facebook.com/" className="socialMediaIcon facebook">
                            <i className="fa fa-facebook"></i>
                        </a>
                    </li>
                    <li className="socialMediaAlign">
                        <a href="http://www.google.com/" className="socialMediaIcon google-plus">
                            <i className="fa fa-google-plus"></i>
                        </a>
                    </li>
                    <li className="socialMediaAlign">
                        <a href="http://linkedin.com/" className="socialMediaIcon linkedin">
                            <i className="fa fa-linkedin"></i>
                        </a>
                    </li>
                    <li className="socialMediaAlign">
                        <a href="http://www.twitter.com/" className="socialMediaIcon twitter">
                            <i className="fa fa-twitter"></i>
                        </a>
                    </li>
                    <li className="socialMediaAlign">
                        <a href="http://www.gmail.com/" className="socialMediaIcon email">
                            <i className="fa fa-envelope"></i>
                        </a>
                    </li>
                    <li className="socialMediaAlign">
                        <a href="http://youtube.com/" target="blank" className="socialMediaIcon youtube">
                            <i className="fa fa-youtube"></i>
                        </a>
                    </li>
                </ul>
                
                {/* footer copyright and team name */}
                <div className="footerLast">
                    <Copyright />
                </div>
            </div>
        );
    }
}

export default footer;