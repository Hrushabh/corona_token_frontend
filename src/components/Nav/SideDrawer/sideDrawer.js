import React from 'react';
import './sideDrawer.css';

const sideDrawer = props => {
    let drawerClass = ['side-drawer'];
    if(props.show){
        drawerClass = ['side-drawer', 'open']
    }
    return (
    <nav className={drawerClass.join(' ')}>
        <ul>
            <li><a href="/home">Home</a></li>
            <li><a href="/about">About</a></li>
            <li><a href="/register">Register</a></li>
            <li><a href="/login">Login</a></li>
        </ul>
    </nav>
    );
};

export default sideDrawer;