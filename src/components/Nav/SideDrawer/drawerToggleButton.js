import React from 'react';
import './drawerToggleButton.css'
const drawerToggleButton =  props => {

    let buttonToggle1 = ['line1'];
    let buttonToggle2 = ['line2'];
    let buttonToggle3 = ['line3'];
    if(props.temp1){
        buttonToggle1 = 'toggle1 line1';
        buttonToggle2 = 'toggle2 line2';
        buttonToggle3 = 'toggle3 line3';
    }

    return (
    <div className="toggle-button" onClick={props.click}>
        <div className={buttonToggle1}></div>
        <div className={buttonToggle2}></div>
        <div className={buttonToggle3}></div>
    </div>
    );
};

export default drawerToggleButton;