import React, { Component } from 'react';
import './navbar.css';
import DrawerToggleButton from '../SideDrawer/drawerToggleButton';
import DataService from '../../../services/service';
import AssignmentIndIcon from '@material-ui/icons/AssignmentInd';
class navbar extends Component{
    constructor(props) {
        super(props);
        this.logOut = this.logOut.bind(this);
    
        this.state = {
          currentUser: undefined
        };
      }
    
      componentDidMount() {
        const user = DataService.getCurrentUser();
    
        if (user) {
          this.setState({
            currentUser: user
          });
        }
      }
    
      logOut() {
        DataService.logout();
      }

    render() {
        const { currentUser} = this.state;
        return(
            <header className="headerForNav">
                <nav className="classForNav">
                    <div className="logoForHeading">
                        <h4>CoronaToken</h4>
                    </div>
                    
                        { currentUser ? (
                            <ul className="navClassForLinks">
                            <li><a href="/home">Home</a></li>
                            <li><a href="/about">About</a></li>
                            <li><a href="/home"><AssignmentIndIcon/>{currentUser.name}</a></li>
                            <li><a href="/home"  onClick={this.logOut}>LogOut</a></li>
                            </ul>
                        ) : (
                            <ul className="navClassForLinks">
                            <li><a href="/home">Home</a></li>
                            <li><a href="/about">About</a></li>
                            <li><a href="/register">Register</a></li>
                            <li><a href="/login">Login</a></li>
                            </ul>
                        )}
                    {/* <div className="display-toggle-button">
                        <DrawerToggleButton click={props.drawerClickHandler} temp1={props.temp}/>
                    </div> */}
                </nav>
            </header>
        );
    }
}

export default navbar;