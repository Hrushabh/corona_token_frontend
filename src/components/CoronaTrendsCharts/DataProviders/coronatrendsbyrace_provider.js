
const data = [
    {
      "id": "java",
      "label": "java",
      "value": 508,
      "color": "hsl(160, 70%, 50%)"
    },
    {
      "id": "sass",
      "label": "sass",
      "value": 166,
      "color": "hsl(20, 70%, 50%)"
    },
    {
      "id": "erlang",
      "label": "erlang",
      "value": 510,
      "color": "hsl(348, 70%, 50%)"
    },
    {
      "id": "go",
      "label": "go",
      "value": 97,
      "color": "hsl(277, 70%, 50%)"
    },
    {
      "id": "stylus",
      "label": "stylus",
      "value": 128,
      "color": "hsl(196, 70%, 50%)"
    }
  ]

  export { data };