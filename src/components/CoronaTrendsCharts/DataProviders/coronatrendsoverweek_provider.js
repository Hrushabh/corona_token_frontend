const data = [
    {
      "id": "japan",
      "color": "hsl(17, 70%, 50%)",
      "data": [
        {
          "x": "plane",
          "y": 131
        },
        {
          "x": "helicopter",
          "y": 240
        },
        {
          "x": "boat",
          "y": 51
        },
        {
          "x": "train",
          "y": 155
        },
        {
          "x": "subway",
          "y": 277
        },
        {
          "x": "bus",
          "y": 55
        },
        {
          "x": "car",
          "y": 61
        },
        {
          "x": "moto",
          "y": 84
        },
        {
          "x": "bicycle",
          "y": 227
        },
        {
          "x": "horse",
          "y": 24
        },
        {
          "x": "skateboard",
          "y": 20
        },
        {
          "x": "others",
          "y": 293
        }
      ]
    },
    {
      "id": "france",
      "color": "hsl(275, 70%, 50%)",
      "data": [
        {
          "x": "plane",
          "y": 95
        },
        {
          "x": "helicopter",
          "y": 228
        },
        {
          "x": "boat",
          "y": 122
        },
        {
          "x": "train",
          "y": 148
        },
        {
          "x": "subway",
          "y": 284
        },
        {
          "x": "bus",
          "y": 284
        },
        {
          "x": "car",
          "y": 252
        },
        {
          "x": "moto",
          "y": 230
        },
        {
          "x": "bicycle",
          "y": 277
        },
        {
          "x": "horse",
          "y": 30
        },
        {
          "x": "skateboard",
          "y": 111
        },
        {
          "x": "others",
          "y": 57
        }
      ]
    },
    {
      "id": "us",
      "color": "hsl(136, 70%, 50%)",
      "data": [
        {
          "x": "plane",
          "y": 84
        },
        {
          "x": "helicopter",
          "y": 250
        },
        {
          "x": "boat",
          "y": 248
        },
        {
          "x": "train",
          "y": 149
        },
        {
          "x": "subway",
          "y": 90
        },
        {
          "x": "bus",
          "y": 228
        },
        {
          "x": "car",
          "y": 136
        },
        {
          "x": "moto",
          "y": 86
        },
        {
          "x": "bicycle",
          "y": 245
        },
        {
          "x": "horse",
          "y": 103
        },
        {
          "x": "skateboard",
          "y": 298
        },
        {
          "x": "others",
          "y": 41
        }
      ]
    },
    {
      "id": "germany",
      "color": "hsl(171, 70%, 50%)",
      "data": [
        {
          "x": "plane",
          "y": 299
        },
        {
          "x": "helicopter",
          "y": 177
        },
        {
          "x": "boat",
          "y": 178
        },
        {
          "x": "train",
          "y": 0
        },
        {
          "x": "subway",
          "y": 118
        },
        {
          "x": "bus",
          "y": 102
        },
        {
          "x": "car",
          "y": 190
        },
        {
          "x": "moto",
          "y": 281
        },
        {
          "x": "bicycle",
          "y": 298
        },
        {
          "x": "horse",
          "y": 246
        },
        {
          "x": "skateboard",
          "y": 74
        },
        {
          "x": "others",
          "y": 202
        }
      ]
    },
    {
      "id": "norway",
      "color": "hsl(217, 70%, 50%)",
      "data": [
        {
          "x": "plane",
          "y": 98
        },
        {
          "x": "helicopter",
          "y": 233
        },
        {
          "x": "boat",
          "y": 175
        },
        {
          "x": "train",
          "y": 263
        },
        {
          "x": "subway",
          "y": 17
        },
        {
          "x": "bus",
          "y": 224
        },
        {
          "x": "car",
          "y": 51
        },
        {
          "x": "moto",
          "y": 64
        },
        {
          "x": "bicycle",
          "y": 221
        },
        {
          "x": "horse",
          "y": 90
        },
        {
          "x": "skateboard",
          "y": 195
        },
        {
          "x": "others",
          "y": 252
        }
      ]
    }
  ];

  export {data};