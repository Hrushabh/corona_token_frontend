import React, {Component} from 'react';
import {Row, Col, CardImg } from "shards-react";
import { Card, CardBody, CardTitle, CardSubtitle } from "shards-react";
import CoronaTrendsByRace from './CoronaTrendsCharts/coronaTrendsByRace.js';
import CoronaTrendsByAge from './CoronaTrendsCharts/CoronaTrendsByAge.js';
import CoronaTrendsOverWeek from './CoronaTrendsCharts/CoronaTrendsOverWeek.js';
import CoronaInfo from './images/CoronaVirusMap.png';

class CoronaTrendsComponent extends Component {
  constructor(){
    super();
    this.state = {};
  }

  render(){
    return(
      <div>
        <h3> Corona Trends </h3>
        <p>These are the latest Covid-19 trends in Los Angeles.</p>
        <Row>
          <Col sm="12" md = "7" lg = "7">
            <Card>
              <CardBody>
              <CardTitle>Progression of Corona Cases in Los Angeles in the past week.</CardTitle><br />
              <CardSubtitle>These are the latest Covid-19 trends in Los Angeles</CardSubtitle>
              Always wear masks as a social responsibility.
              <CoronaTrendsOverWeek />
              </CardBody>
            </Card>
          </Col><br></br>
          <Col sm = "12" md = "5" lg = "5">
            <Card>
              <CardBody>
                <CardTitle>Distribution of Corona Cases by Race</CardTitle><br />
                <CardSubtitle>These are the latest Covid-19 trends in Los Angeles</CardSubtitle>
                Always wear masks as a social responsibility.
                <CoronaTrendsByRace />
              </CardBody>
            </Card>
          </Col><br></br>
        </Row><br></br>
        <Row>
          <Col sm="12" md = "4" lg = "4">
            <Card>
              
              <CardBody>
              <CardTitle>Total Cases: 168,757</CardTitle>
              <CardTitle>Total Deaths: 4,300</CardTitle><br />
              <CardSubtitle>These are the latest Covid-19 trends in Los Angeles</CardSubtitle>
              Always wear masks as a social responsibility.
              </CardBody>
              <CardImg bottom src = {CoronaInfo} />
            </Card>
          </Col><br></br>
          <Col sm = "12" md = "8" lg = "8">
            <Card>
            <CardBody>
              <CardTitle>Corona distribution across age groups in Los Angeles</CardTitle><br />
              <CardSubtitle>These are the latest Covid-19 trends in Los Angeles</CardSubtitle>
              Always wear masks as a social responsibility.
              </CardBody>
              <CoronaTrendsByAge /><br></br>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default CoronaTrendsComponent;