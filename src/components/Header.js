import React, {Component} from 'react';
import SideDrawer from '../components/Nav/SideDrawer/sideDrawer';
import Navbar from '../components/Nav/navbar/navbar';
import Backdrop from '../components/Nav/Backdrop/Backdrop';


class Header extends Component{
  constructor(){
    super();
    this.state = {
      sideDrawerOpen: false,
    };
  }

  drawerToggleClickHandler = () => {
    this.setState((prevState) => {
      return {sideDrawerOpen: !prevState.sideDrawerOpen}
    });
    };
  
  backdropClickHandler = () => {
    this.setState({sideDrawerOpen: false});
  };

  render(){
    let backdrop;
    if(this.state.sideDrawerOpen){
      backdrop = <Backdrop click={this.backdropClickHandler} />
    }
    return(
      <div>
        <link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet"></link>
        <div style={{height: '100%'}}>
            <Navbar drawerClickHandler={this.drawerToggleClickHandler} temp={this.state.sideDrawerOpen}/>
            <SideDrawer show={this.state.sideDrawerOpen}/>
        </div>
      </div>
    );
  }
}

export default Header;