import React, { useState } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
//import FormControlLabel from '@material-ui/core/FormControlLabel';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import PersonAddRoundedIcon from '@material-ui/icons/PersonAdd';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import DataService from "../services/service";

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="/">
        Team LASH
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

function validateEmail(email) {
  const re = /\S+@\S+\.\S+/;
  return re.test(String(email).toLowerCase());
}

function validateMobile(mobileNumber)
{
  const re = 	/^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$/;
  return re.test(String(mobileNumber));
}


const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    //backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const Input = props => (
  <TextField
      error={ props.error }
      label= { props.label }
      value={ props.value }
      onChange={ props.onChangeFunc }
      helperText={ props.error ? props.errMsg : props.normalMsg}
      variant="outlined"
      fullWidth
  />
);

export default function SignUp(props) {
  const classes = useStyles();

      ////////////////////////  Defination for fields /////////////////////
      const [name, setName] = useState('');
      const [nameErr, setNameErr] = useState(false);
      const [email, setEmail] = useState('');
      const [emailErr, setEmailErr] = useState(false);
      const [password, setPassword] = useState('');
      const [passwordErr, setPasswordErr] = useState(false);
      const [mobile, setMobile] = useState('');
      const [mobileErr, setMobileErr] = useState(false);
      const [address, setAddress] = useState('');
      const [addressErr, setAddressErr] = useState(false);

    ///////////////////// SIGN UP FUNCTIONALITY /////////////////////////
    function handleClick(e) {
      e.preventDefault();
      if (!name.trim() || !validateEmail(email.trim()) || !password.trim() || !validateMobile(mobile.trim()) || !address.trim()) 
      {
        if (!name.trim()) setNameErr(true);
        else setNameErr(false);
        if (!validateEmail(email.trim())) setEmailErr(true);
        else setEmailErr(false);
        if (!password.trim()) setPasswordErr(true);
        else setPasswordErr(false);
        if(!validateMobile(mobile.trim())) setMobileErr(true);
        else setMobileErr(false);
        if (!address.trim()) setAddressErr(true);
        else setAddressErr(false);
      }
      else {
        var data = {
          name: name,
            email: email,
            password: password,
            mobile: mobile,
            address: address
        }
        DataService.signup(data)
            .then(response => {
                console.log(response.data);
                props.history.push("/login");
        })
        .catch(e => {
            alert("Email already exists!")
            console.log(e);
        });
    }
    }
    ///////////////////////////////////////////////////////////////////


  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <PersonAddRoundedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign up
        </Typography>
        <form className={classes.form} noValidate autoComplete="off">
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <Input 
                error={nameErr} 
                label="Name"
                value={ name } 
                onChangeFunc={e=>setName(e.target.value)}
                errMsg="Please enter Name"
                normalMsg=""
                autoFocus
              />
            </Grid>
            <Grid item xs={12}>
              <Input 
                error={emailErr} 
                label="Email Address"
                value={ email } 
                onChangeFunc={e=>setEmail(e.target.value)}
                errMsg="Please enter a valid email address"
                normalMsg=""
              />
            </Grid>
            <Grid item xs={12}>
              <Input 
                type="password"
                error={passwordErr} 
                label="Password"
                value={ password } 
                onChangeFunc={e=>setPassword(e.target.value)}
                errMsg="Please enter password"
                normalMsg=""
                />
              </Grid>
              <Grid item xs={12}>
                <Input 
                error={mobileErr} 
                label="Mobile"
                value={ mobile } 
                onChangeFunc={e=>setMobile(e.target.value)}
                errMsg="Please enter Mobile number"
                normalMsg=""
                />
            </Grid>
            <Grid item xs={12}>
              <Input 
                error={addressErr} 
                label="Address"
                value={ address } 
                onChangeFunc={e=>setAddress(e.target.value)}
                errMsg="Please enter Address"
                normalMsg=""
              />
            </Grid>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            onClick={handleClick}
          >
            Sign Up
          </Button>
          <Grid container justify="flex-end">
            <Grid item>
              <Link href="/login" variant="body2">
                Already have an account? Sign in
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
      <Box mt={5}>
        <Copyright />
      </Box>
    </Container>
  );
}